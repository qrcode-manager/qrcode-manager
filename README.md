qrcode-manager
==============

This software helps managing QR Codes (http://www.denso-wave.com/qrcode/index-e.html), allowing easy generation of QR Codes suitable for print or web formats and management of redirection to final targets via Apache mod_rewrite.

Installation
------------

This script depends on the `rqr` and `json` Ruby gems.

As of time of writing, the `rqr` gem seems to compile correctly for Ruby 1.8 only - YMMV.

Usage
-----

To generate a batch of QR codes, first create a JSON configuration file which is used to configure:

* the batch id `batch_id` (all EPS files generated will be stored in a single folder for easy export); this must be a single string using only alphanumeric characters, hyphens ('-'), underscores ('_') - no spaces or any other character (e.g. `my-qrcode-test`)
* the base URI `base_uri` (e.g. `https://example.com/`)
* the base folder `base_folder` (e.g. `shortcodes/`)
* the shortcodes, the redirection targets and (optionally) the 3xx HTTP status code to be used for the redirect (e.g. `["95ed", "https://openstreetmap.org/", 302]`)

Note that the base URI is combined with each shortcode in the batch to generate the QR Code and as such should not be changed once the QR Codes have been generated; the redirection target can be changed at any time to accommodate changes in information to be served to visitors (but if a 301 HTTP status code has been used for a specific redirection, visitors who have used the corresponding QR Code before might not get redirected to the new target).

Example configuration file:

    {
      "qrcode-manager": 1,
      "base_uri": "https://example.com/",
      "base_folder": "shortcodes/",
      "batch_id": "my-qrcode-test",
      "codes": [
        [ "abd0", "/about-us/", 307 ],
        [ "cfx0", "https://openstreetmap.org/", 302 ],
        [ "95ed", "/contact-us/" ]
      ]
    }

Generating QR Code EPS files
----------------------------

Assuming the configuration file described above has been created correctly and saved as `my-qrcode-test.json`, to generate all the EPS files required run:

`ruby1.8 qrcode-manager [configuration-file]` - i.e.
`ruby1.8 qrcode-manager my-qrcode-test.json`

The script will create a folder named `my-qrcodes-test` (as defined in the JSON file) and save in there all the EPS files required, alongside an .htaccess file that can be used with Apache 2.

Bugs & limitations
------------------

This is my first ever Ruby script - basically, it's playground code and it probably is written with a strong Perl "accent" rather than in idiomatic Ruby.

It doesn't have tests and it doesn't much input validation. This is *very* bad, but the code as is works fine for my first use case. Pull requests welcome.

Currently there is no way to fine-tune the generated QR Codes through the flexible configuration options of the `rqr` gem, e.g. QR Code level, file format other than EPS, etc.

The script has been tested only under Debian and Ubuntu.

Each time the script is run, it overwrites any EPS files that might already exist in the target folder - this shouldn't be a problem because you'll be using git to version the configuration files anyways.

As it uses JSON configuration files with no central storage, there is no way to guarantee that a shortcode is unique. If you need to check this, store your configurations in CouchBase, run any MapReduce stuff to check for duplicates, and generate the configuration files for qrcode-manager via `curl`.

License
-------

Copyright (C) 2011 Xelera

Author: Andrea Rota <a@xelera.eu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
